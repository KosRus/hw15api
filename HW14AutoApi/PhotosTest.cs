﻿using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW14AutoApi
{
    class PhotosTest
    {
        [Test]
        public static void CreateGetInPhotos()
        {
            RestApiHelper<Posts> restApi = new RestApiHelper<Posts>();
            var restUrl = restApi.SetUrl("/albums/1/photos/");
            var restRequest = restApi.CreateGetRequest();
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Photos content = restApi.GetContents<Photos>(response)[0];
            Console.WriteLine(response.StatusCode);
            Assert.AreEqual("OK", response.StatusCode.ToString());
            Assert.AreEqual("accusamus beatae ad facilis cum similique qui sunt", content.title);
            Assert.AreEqual("https://via.placeholder.com/150/92c952", content.thumbnailUrl);
            Assert.AreEqual("https://via.placeholder.com/600/92c952", content.url);
        }

        [Test]
        public static void CreatePostInPhotos()
        {
            string jsonString = @"{
                ""title"": ""Photos"",
                ""thumbnailUrl"": ""PhotosTime"",
                ""url"": ""3""
              }";
            RestApiHelper<Photos> restApi = new RestApiHelper<Photos>();
            var restUrl = restApi.SetUrl("/albums/3/photos/");
            var restRequest = restApi.CreatePostRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Photos content = restApi.GetContent<Photos>(response);

            Assert.AreEqual("Created", response.StatusCode.ToString());
            Assert.AreEqual("Photos", content.title);
            Assert.AreEqual("PhotosTime", content.thumbnailUrl);
            Assert.AreEqual("3", content.url);
        }

        [TestCase("3", "5001", "urlTest", "TextUrl1")]
        [TestCase("3", "5001", "Testerovskiy", "TextUrl2")]
        [TestCase("3", "5001", "Tuster", "TextUrl3")]
        [TestCase("3", "5001", "TestTime", "TextUrl4")]
        public static void PostToPhotosDictionary(string albomIdTest, string IdTest, string urlTest, string thumbnaiTest)
        {
            Dictionary<string, string> body = new Dictionary<string, string>

            {
                {"albumId", albomIdTest},
                {"id" , IdTest},
                {"url", urlTest},
                {"thumbnailUrl", thumbnaiTest}
            };
            RestApiHelper<Photos> restApi = new RestApiHelper<Photos>();
            var restUrl = restApi.SetUrl("/albums/3/photos/");
            var restRequest = restApi.CreatePostRequestDictionary(body);
            var response = restApi.GetResponse(restUrl, restRequest);
            Photos content = restApi.GetContent<Photos>(response);

            Assert.AreEqual("Created", response.StatusCode.ToString());
            Assert.AreEqual(albomIdTest, content.albumId);
            Assert.AreEqual(IdTest, content.id);
            Assert.AreEqual(urlTest, content.url);
            Assert.AreEqual(thumbnaiTest, content.thumbnailUrl);
        }

        [Test]
        public static void PutDataInAlbums()
        {
            string jsonString = @"{
                ""title"": ""TextTestika"",
                ""thumbnailUrl"": ""11""
              }";
            RestApiHelper<Photos> restApi = new RestApiHelper<Photos>();
            var restUrl = restApi.SetUrl("/albums/3/photos/");
            var restRequest = restApi.CreatePutRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Photos content = restApi.GetContent<Photos>(response);
            Assert.AreEqual(null, content.title);
            Assert.AreEqual(null, content.thumbnailUrl);
        }


    }
}
