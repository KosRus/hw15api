﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW14AutoApi
{
    public class Users
    {
        public int id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string adress { get; set; }
        public string phone { get; set; }
        public string website { get; set; }
        public string company { get; set; }
    }
}
