﻿using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW14AutoApi
{
    [TestFixture]
    public class PostsTest
    {
        [Test]
        public static void CreatePostInPosts()
        {
            string jsonString = @"{
                ""title"": ""TestTestovi4"",
                ""body"": ""DelaemTest"",
                ""userId"": ""3""
              }";
            RestApiHelper<Posts> restApi = new RestApiHelper<Posts>();
            var restUrl = restApi.SetUrl("/posts");
            var restRequest = restApi.CreatePostRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Posts content = restApi.GetContent<Posts>(response);

            Assert.AreEqual("Created", response.StatusCode.ToString());
            Assert.AreEqual("TestTestovi4", content.title);
            Assert.AreEqual("DelaemTest", content.body);
            Assert.AreEqual("3", content.userId);
        }

        [TestCase("15", "101", "Test Testovi4")]
        [TestCase("16", "101", "Testerovskiy")]
        [TestCase("17", "101", "Tuster")]
        [TestCase("18", "101", "Booster")]

        public static void PostToPostsDictionary(string userIdTest, string idTest, string titleTest)
        {
            Dictionary<string, string> body = new Dictionary<string, string>

            {
                {"userId" , userIdTest},
                {"id", idTest},
                {"title", titleTest}

            };
            RestApiHelper<Posts> restApi = new RestApiHelper<Posts>();
            var restUrl = restApi.SetUrl("/posts");
            var restRequest = restApi.CreatePostRequestDictionary(body);
            var response = restApi.GetResponse(restUrl, restRequest);
            Posts content = restApi.GetContent<Posts>(response);

            Assert.AreEqual("Created", response.StatusCode.ToString());
            Assert.AreEqual(userIdTest, content.userId);
            Assert.AreEqual(idTest, content.id);
            Assert.AreEqual(titleTest, content.title);

        }
        [Test]
        public static void GetPostFromPosts()
        {
            RestApiHelper<Posts> restApi = new RestApiHelper<Posts>();
            var restUrl = restApi.SetUrl("/posts/1");
            var restRequest = restApi.CreateGetRequest();
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Posts content = restApi.GetContent<Posts>(response);
            Assert.AreEqual("sunt aut facere repellat provident occaecati excepturi optio reprehenderit", content.title);
            Assert.AreEqual("quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto", content.body);
            Assert.AreEqual("1", content.userId);
        }

        [Test]
        public static void PutDataInPosts()
        {
            string jsonString = @"{
                ""title"": ""TestString"",
                ""body"": ""TestingInfo"",
                ""userId"": 13
              }";
            RestApiHelper<Posts> restApi = new RestApiHelper<Posts>();
            var restUrl = restApi.SetUrl("/posts/1");
            var restRequest = restApi.CreatePutRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Posts content = restApi.GetContent<Posts>(response);
            Assert.AreEqual("TestString", content.title);
            Assert.AreEqual("TestingInfo", content.body);
            Assert.AreEqual("13", content.userId);
        }
        [Test]
        public static void DeleteFromPhotos()
        {
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/posts/2");
            var restRequest = restApi.CreateDeleteRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Posts content = restApi.GetContent<Posts>(response);


            Assert.AreEqual("OK", response.StatusCode.ToString());
            Assert.AreEqual(null, content.userId);
            Assert.AreEqual(null, content.id);
            Assert.AreEqual(null, content.title);
        }
    }
}
