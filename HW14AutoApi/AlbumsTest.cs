﻿using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW14AutoApi
{
    [TestFixture]
    class AlbumsTest
    {
        [Test]
        public static void GetDataFromAlbums()
        {

            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/Albums/1");
            var restRequest = restApi.CreateGetRequest();

            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);

            Assert.AreEqual("quidem molestiae enim", content.title);
            Assert.AreEqual("1", content.userId);
        }

        [Test]
        public static void PatchDataToAlbums()
        {
            string jsonString = @"{""title"": ""test string""}";
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums/5");
            var restRequest = restApi.CreatePatchRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);

            Assert.AreEqual("OK", response.StatusCode.ToString());
            Assert.AreEqual("test string", content.title);
            Assert.AreEqual("1", content.userId);
            Assert.AreEqual("5", content.id);
        }

        [Test]
        public static void CreatePostInAlbums()
        {
            string jsonString = @"{
                ""title"": ""TestTestovi4"",
                ""userId"": ""10""
              }";
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/Albums");
            var restRequest = restApi.CreatePostRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);

            Assert.AreEqual("Created", response.StatusCode.ToString());
            Assert.AreEqual("TestTestovi4", content.title);
            Assert.AreEqual("10", content.userId);
        }
        [TestCase("2", "101", "Test Testovi4")]
        [TestCase("3", "101", "Testerovskiy")]
        [TestCase("1", "101", "Tuster")]
        [TestCase("55", "101", "Booster")]

        public static void PostToAlbumsDictionary(string userIdTest, string idTest, string titleTest)
        {
            Dictionary<string, string> body = new Dictionary<string, string>

            {
                {"userId" , userIdTest},
                {"id", idTest},
                {"title", titleTest}
            };
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/Albums");
            var restRequest = restApi.CreatePostRequestDictionary(body);
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);

            Assert.AreEqual("Created", response.StatusCode.ToString());
            Assert.AreEqual(userIdTest, content.userId);
            Assert.AreEqual(idTest, content.id);
            Assert.AreEqual(titleTest, content.title);
        }

        [Test]
        public static void PutDataInAlbums()
        {
            string jsonString = @"{
                ""title"": ""TextTestika"",
                ""userId"": ""19""
              }";
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/Albums/5");
            var restRequest = restApi.CreatePutRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);
            Assert.AreEqual("TextTestika", content.title);
            Assert.AreEqual("19", content.userId);
        }
        [Test]
        public static void DeleteFromAlbums()
        {
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums/3");
            var restRequest = restApi.CreateDeleteRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);
            Assert.AreEqual("OK", response.StatusCode.ToString());
            Assert.AreEqual(null, content.userId);
            Assert.AreEqual(null, content.id);
            Assert.AreEqual(null, content.title);
        }
    }
}
