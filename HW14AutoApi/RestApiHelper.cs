﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW14AutoApi
{
    public class RestApiHelper<T>
    {
        public RestClient _restClient;
        public RestRequest _restRequest;
        public string _baseUrl = "https://jsonplaceholder.typicode.com";

        public RestClient SetUrl(string resourceUrl)
        {
            RestClient _restClient = new RestClient(_baseUrl + resourceUrl);
            return _restClient;
        }
        /// <summary>
        /// Создание POST запроса
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public RestRequest CreatePostRequest(string jsonString)
        {
            _restRequest = new RestRequest(Method.POST);
            _restRequest.AddHeader("Accept", "application/json");
            _restRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);
            return _restRequest;
        }
        /// <summary>
        /// Создание PUT запроса
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public RestRequest CreatePutRequest(string jsonString)
        {
            _restRequest = new RestRequest(Method.PUT);
            _restRequest.AddHeader("Accept", "application/json");
            _restRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);
            return _restRequest;
        }
        /// <summary>
        /// Создания PATCH запроса
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public RestRequest CreatePatchRequest(string jsonString)
        {
            _restRequest = new RestRequest(Method.PATCH);
            _restRequest.AddHeader("Accept", "application/json");
            _restRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);
            return _restRequest;
        }
        /// <summary>
        /// Создание GET запроса
        /// </summary>
        /// <returns></returns>
        public RestRequest CreateGetRequest()
        {
            _restRequest = new RestRequest(Method.GET);
            _restRequest.AddHeader("Accept", "application/json");
            return _restRequest;
        }
        /// <summary>
        /// Создание DELETE запроса
        /// </summary>
        /// <returns></returns>
        public RestRequest CreateDeleteRequest()
        {
            _restRequest = new RestRequest(Method.DELETE);
            _restRequest.AddHeader("Accept", "application/json");
            return _restRequest;
        }
        /// <summary>
        /// Метод отправляет запрос к Интернет-ресурсу и возвращает WebResponse экземпляр.
        /// </summary>
        /// <param name="restClient"></param>
        /// <param name="restRequest"></param>
        /// <returns></returns>
        public IRestResponse GetResponse(RestClient restClient, RestRequest restRequest)
        {
            return restClient.Execute(restRequest);
        }
        /// <summary>
        /// Создание POST запроса с использованием Словаря
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        public RestRequest CreatePostRequestDictionary(Dictionary<string, string> body)
        {
            _restRequest = new RestRequest(Method.POST);
            _restRequest.AddHeader("Accept", "application/json");
            _restRequest.AddJsonBody(body);
            return _restRequest;
        }
        /// <summary>
        /// Метод вернет содержимое блока (уже добавленное на страницу и возможно измененное пользователем).
        /// </summary>
        /// <typeparam name="DTO"></typeparam>
        /// <param name="response"></param>
        /// <returns></returns>
        public List<DTO> GetContents<DTO>(IRestResponse response)
        {
            var content = response.Content;
            List<DTO> deseiralizeObject = (List<DTO>) Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<DTO>>(content);
            return deseiralizeObject;
        }

        public DTO GetContent<DTO>(IRestResponse response)
        {
            var content = response.Content;
            DTO deseiralizeObject = Newtonsoft.Json.JsonConvert.DeserializeObject<DTO>(content);
            return deseiralizeObject;
        }
    }
}

